## Helm-PerfectApp

Репозиторий содержит Helm-чарт для приложения [PerfectApp](https://gitlab.com/la-test/perfectapp), а также манифесты для Tiller и Cluster Issuer.

Все необходимые для правки переменные отражены в файле `charts/perfectapp/values.yaml`. 

По умолчанию, в качестве ingress controller в данном чарте используется nginx. Выпуск сертификатов Let's Encrypt выполняется при помощи cert-manager и Cluster Issuer.

Для корректного деплоя приложения необходимо инициализировать значения переменных окружения в настройках CI/CD проекта:
- `DOMAIN` (доменное имя)
- `EXTERNAL_LB_IP` (статический IP внешнего балансировщика)

С целью проверки работы приложения в GCP для проекта `prod-task` в регионе `us-central1` зарезервирован статический IP `104.154.169.93`, в качестве доменного имени используется wildcard domain `104-154-169-93.sslip.io`.

Ссылки на приложения. запущенные в различных kubernetes namespaces (также доступны в GitLab CI через меню environments):
* https://staging.104-154-169-93.sslip.io
* https://production.104-154-169-93.sslip.io

### Pipeline 
Pipeline приложения состоит из трех стадий:

 1. **Test**. Проверка синтаксиса Helm chart
 2. **Staging**. Деплой чарта PerfectApp в "staging" Kubernetes namespace из ветки `master`
 3. **Release**. Деплой чарта PerfectApp в "production" Kubernetes namespace из ветки `master` (запуск данной стадии выполняется только вручную)

Для корректной работы helm требуется выполнить авторизацию в созданном ранее с помощью Ansible кластере Kubernetes.
Для этого в качестве временного решения в окружение проекта [Helm-PerfectApp](https://gitlab.com/la-test/helm-perfectapp) GitLab добавлены слeдующие переменные:

- `CI_GCP_PROD_ENV_SERVICE_KEY`
- `CI_GKE_PROD_CLUSTER_NAME`
- `CI_GKE_PROD_CLUSTER_ZONE`
- `CI_GKE_PROD_CLUSTER_PROJECT`

Данные переменные статичны (кроме первой), поэтому создание кластера в проекте `prod-task` с другими параметрами (например, с другим регионом размещения нод) приведет к полной неработоспособности данного pipeline, по этой причине озвученный подход не может быть применен при промышленной эксплуатации, для передачи сертификатов авторизации из pipeline проекта [ansible-gke-setup](https://gitlab.com/la-test/ansible-gke-setup) в текущий следует использовать другие методы.

### TODO
1. Реализовать проверку наличия HTTP L7 Load Balancer Add-on в кластере и, в зависимости от результатов, выбирать тот или иной тип ingress-controller (nginx либо GCLB)
2. Связать данный pipeline с pipline основного приложения.
3. Зафиксировать версии сторонних утилит для сборки и helm-чартов для предотвращения незапланированных проблем при использовании stable/latest-версий

